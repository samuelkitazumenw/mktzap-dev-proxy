const axios = require('axios')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const methods = require('./methods');

const app = express()
const PORT = process.env.PORT || 3001
const HOST = process.env.HOST || '127.0.0.1'
const MKTZAP_UPLOAD_API = process.env.MKTZAP_UPLOAD_API
const MKTZAP_API = process.env.MKTZAP_API

if (!MKTZAP_API) {
  console.error(new Error())
  process.exit(0)
}

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.route('*')
  .get(cors(), methods.get(MKTZAP_UPLOAD_API))
  .post(cors(), methods.post(MKTZAP_UPLOAD_API))
  .patch(cors(), methods.patch(MKTZAP_UPLOAD_API))

app.route('/api/*')
  .get(cors(), methods.get(MKTZAP_API))
  .post(cors(), methods.post(MKTZAP_API))
  .patch(cors(), methods.patch(MKTZAP_API))

app.listen(PORT, HOST)
console.log(`MKTZAP API Proxy running on ${HOST}:${PORT}`);
console.log(`...proxying API on ${MKTZAP_API}`);
console.log(`...proxying UPLOAD API on ${MKTZAP_UPLOAD_API}`);