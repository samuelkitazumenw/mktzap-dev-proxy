const axios = require('axios');

module.exports = (uri) => async (req, res) => {
  try {
    const { headers, body, path } = req
    console.log(`[INCOMING] PATCH ${path}`)
    console.log(`[INCOMING] - BODY`)
    Object.keys(body).forEach((v) => {
      console.log(`[INCOMING]    - ${v}: ${body[v]}`)
    })
    console.log(`[INCOMING] - HEADERS`)
    console.log(`[INCOMING]     - ${JSON.stringify(req.headers)}`)
    const axiosHeader = headers.authorization ? { "authorization": headers.authorization } : {}
    const data = await axios({
      method: 'PATCH',
      url: `${uri}${path}`,
      data: body,
      headers: axiosHeader
    })
    console.log(`[OUTCOMING] PATCH ${uri}${path}`)
    console.log(`[OUTCOMING] - HEADERS ${JSON.stringify(axiosHeader)}`)
    res.json(data.data)
  } catch (e) {
    console.error(e)
    res.send('ERROR')
  }
};