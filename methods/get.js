const axios = require('axios');

module.exports = (uri) => async (req, res) => {
  try {
    const { headers, path, query } = req
    console.log(`[INCOMING] GET ${path}`)
    console.log(`[INCOMING] - QUERY`)
    Object.keys(query).forEach(v => {
      console.log(`[INCOMING]    - ${v}: ${query[v]}`)
    })
    console.log(`[INCOMING] - HEADERS`)
    console.log(`[INCOMING]     - ${JSON.stringify(req.headers)}`)
    const authorizationHeader = headers.authorization ? { "authorization": headers.authorization } : {}
    const cookieHeader = { Cookie: headers.mktzapcookie || '' }
    const axiosHeader = Object.assign({}, authorizationHeader, cookieHeader)
    const data = await axios({
      method: 'GET',
      url: `${uri}${path}`,
      headers: axiosHeader
    })
    console.log(`[OUTCOMING] GET ${uri}${path}`)
    console.log(`[OUTCOMING] - HEADERS ${JSON.stringify(axiosHeader)}`)
    res.json(data.data)
  } catch (e) {
    console.error(e)
    res.send('ERROR')
  }
};