const axios = require('axios');

module.exports = (uri) => async (req, res) => {
  try {
    const { headers, body, path } = req
    console.log(`[INCOMING] POST ${path}`)
    console.log(`[INCOMING] - BODY`)
    Object.keys(body).forEach((v) => {
      console.log(`[INCOMING]    - ${v}: ${body[v]}`)
    })
    console.log(`[INCOMING] - HEADERS`)
    console.log(`[INCOMING]     - ${JSON.stringify(req.headers)}`)
    const authorizationHeader = headers.authorization ? { "authorization": headers.authorization } : {}
    const cookieHeader = { Cookie: headers.mktzapcookie || '' }
    const axiosHeader = Object.assign({}, authorizationHeader, cookieHeader)
    const data = await axios({
      method: 'POST',
      url: `${uri}${path}`,
      data: body,
      headers: axiosHeader
    })
    console.log(`[OUTCOMING] POST ${uri}${path}`)
    console.log(`[OUTCOMING] - HEADERS ${JSON.stringify(axiosHeader)}`)
    res.json(data.data)
  } catch (e) {
    console.error(e)
    res.send('ERROR')
  }
};